var should = require("should");
var fill = require("../lib/coffee-code.js");

describe("Coffee Script Successfully compiled.", function(){
  it("When this passed", function(){
    fill("mug", "coffee").should.equal("Filling the mug with coffee");
  });
});