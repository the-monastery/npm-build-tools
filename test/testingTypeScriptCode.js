var should = require("should");
var greeter = require("../lib/ts-code.js");

describe("Typescript compiled", function(){
  it("When this passed", function(){
    var g = new greeter();
    g.greet("Yoda").should.equal("A type-script greeting to you, Yoda");
  });
});