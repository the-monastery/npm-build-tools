var app = require("../");
var request = require("supertest").agent(app.listen());

describe("Main test", function(){
  it("Welcome message", function(done){
    request
      .get("/")
      .expect("Hello World!")
      .end(done);
  });
});