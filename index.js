var app = module.exports = require("koa")();

var html =
  `<html>
    <head>
      <meta charset="UTF-8">
      <title>A small test page</title>
      <script src="//localhost:9091"></script>
    </head>
    <body>
      Page loaded at: <span id="demo"></span>
    </body>
    <script language="JavaScript">
      document.getElementById('demo').innerHTML = new Date();
    </script>
  </html>`;

app.use(function *(){
  if(this.request.path === "/client"){
    this.body = html;
    return;
  }

  this.body = "Hello World!";
});

var port = process.env.PORT || (process.argv[2] || 3000);
port = (typeof port != "number") ? port : 3000;

if(!module.parent){ app.listen(port); }

console.log("Application Started on port " + port);