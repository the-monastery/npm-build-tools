#!/usr/bin/env bash
npm run deploy:test -s &&
npm run build -s &&
npm run version:patch &&
npm run push:origin &&
npm run push:heroku &&
npm run launch:prod